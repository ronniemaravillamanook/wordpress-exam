<?php
/**
 * Template Name: Static Page
 *
 * @package zerif-lite
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>Haven</title>
	<?php wp_head(); ?>

</head>
<body>
	<header class="entry-header ">
		<div class="row">
			<div class="col col-lg-6">
			
			
			<img src="http://localhost:8080/wordpress-BBTFullStackExam/wp-content/uploads/2018/12/logo.png">
			
			<h1 class="h1">Lorem ipsum dolor sit amet </h1>
			<p> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</p>
		</div>
		<div class="col col-lg-6">
			<h1>News & blog</h1>
			<hr>
			<div>
				<ul>
					<li><a href="">KiwiSaver HomeStart grant explained</a></li>
					<li><a href="">What's happening in the property market?</a></li>
					<li><a href="">Trust law reform</a></li>
					<li><a href="">Tax update - Simplification of taxes, foreign ...</a></li>
					<li><a href="">What do the new lending rules mean?</a></li>
					<li><a href="">How will the Reserve Bank respond to...</a></li>
				</ul>
			</div>
		</div>
		</div>		
	</header>
		<div class="nav">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link" href="#">Book keeping & payroll</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Financial & GST</a></li>
				<li class="nav-item"><a class="nav-link" href="#">KiwiSaver advice</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Insuring people</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Tax & business advice</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Trustee services</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Mortgage advice</a></li>
			</ul>
		</div>
	<div class="content-banner">
		<div class="row">
			<div class="col col-lg-8">
				<img src="http://localhost:8080/wordpress-BBTFullStackExam/wp-content/uploads/2018/12/video-img.png">
			</div>
			<div class="col col-lg-4">
				<h1>Why have a financial adviser?</h1>

				<p>
					Haven was created out of the need to simply do things better. With specialist advisers across a range of financial services, we will offer you the best, practical advice for your unique situation.				
				</p>
				<p>
					Our financial advisers span the north and south island making us conveniently positioned wherever you need us.
				</p>

				<div class="btn-group">
					<button class="btn btn-info pull-right toggle">Learn more about our advisers<i class="arrow-icon"></i></button>				    				 
				</div>
			</div>
		</div>		
	</div>
	<div class="contact-form-7">
		<div class="row">
			<div class="col col-lg-7">
				<?php echo do_shortcode("[contact-form-7 id='22' title='Contact us today']"); ?>
			<div class="col col-lg-5">
				<h1>Contact us</h1>
				<hr>
				<div>
					<ul>
						<li><a href="">Find an adviser</a></li>
						<li><a href="">Talk to an accountant</a></li>
						<li><a href="">Talk to a mortguage broker</a></li>
						<li><a href="">0800 &nbsp 700 &nbsp 699 </a></li>
						<li><a href="">reception@havenadvisers.co.nz</a></li>
						<li><a href="">Be a part of our Facebook community</a></li>
					</ul>
				</div>
			</div>
		</div>		
	</div>
	<div class="body-footer">
		<div class="sign-up-form row">
			<div class="col col-lg-2"></div>
			<div class="col col-lg-8">
				<label>SIGN UP TO OUR NEWSLETTER</label>
				<input type="text" name="fullname" class="fullname" placeholder="Full Name"/>
				<input type="text" name="emailaddress" class="emailaddress" placeholder="Email Address"/>
				<button>Submit</button>
			</div>
			<div class="col col-lg-2"></div>
		</div>	
		<div class="row">
			<div class="col col-lg-12">
				<img src="http://localhost:8080/wordpress-BBTFullStackExam/wp-content/uploads/2018/12/logo.png">
			</div>
		</div>	
		<div class="row">
			<div class="col col-lg-2"></div>
			<div class="col col-lg-8 bottom-nav">
				<span>HOME</span>
				<span>OUR SERVICES</span>
				<span>ABOUT US</span>
				<span>TESTIMONIALS</span>
				<span>BLOG</span>
				<span>CONTACT US</span>
			</div>
			<div class="col col-lg-2"></div>
		</div>
		<div class="row">
			<div class="col col-lg-12 footer-contact-number">
				<span>0800 &nbsp 700 &nbsp 699</span>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-12 footer-email-address">
				<span>reception@havenadvisers.co.nz</span>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-12 footer-copyright">
				<span>Copyright © 2017 Haven Advisers Limited. All rights reserved </span>
			</div>
		</div>

	</div>

</body>
</html>
